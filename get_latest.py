#!/usr/bin/python3

import json
import re
import glob
import os

try:
    # Determine the image stream to select the image from
    stream = os.environ.get("QUERY_STRING", "invalid-stream")
    # Remove invalid characters
    stream = re.sub('[^a-z]','',stream)
    # Truncate the name
    stream = stream[:40]
    # Get a list of files
    files = glob.glob('images/{}-*.jpg'.format(stream))
    # Find the newest file
    latest_file = max(files, key=os.path.getctime)

    # Deliver the response
    print('')
    print(latest_file)
except:
    # Error!
    print('')
    print('error.jpg')