/*!
 *  NPS Cameras Automatic Update Script
 *
 *  Written by Nicholas Hollander <nhhollander@wpi.edu>
 */

function register_updater(element) {
    // Get the stream name
    var sname = element.getAttribute("wc-stream");
    // Get the update interval
    var interval = parseInt(element.getAttribute("wc-interval"));
    // Create the update function
    var update = function() {
        console.log("Updating [" + sname + "]...");
        // Get the update from the server
        var request = new XMLHttpRequest();
        request.onreadystatechange = function() {
            if(this.readyState == 4 && this.status == 200) {
                // Update the image
                console.log("Got response for [" + sname + "]!");
                element.src = request.responseText;
            }
        };
        request.open("GET", "get_latest.py?" + sname);
        request.send();
    };
    // Create the update functin
    setInterval(update, interval);
    // Initial invocation
    update();
}

// Find elements that can be updated
console.log("Finding images");
elements = document.querySelectorAll("img[wc-stream][wc-interval]");
elements.forEach(function(element) {
    // Register the magical update gizmo
    register_updater(element);
    // Register the click handler
    element.addEventListener("click", image_clicked);
});

// Handle an image being clicked
function image_clicked(event) {
    // Get the close frame
    var cframe = document.getElementById("cframe");
    // Check if enlarged
    if(event.target.className.includes("enlarged")) {
        console.log("Collapsing frame");
        // Remove the enlarged attribute
        event.target.className = event.target.className.replace("enlarged","");
        // Hide the frame
        cframe.className = cframe.className.replace("enlarged","");
    } else {
        console.log("Expanding frame");
        // Update the class name
        event.target.className += " enlarged";
        // Show the frame
        cframe.className += " enlarged";
    }
}